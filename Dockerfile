FROM sagemath/sagemath:9.1-py3
USER root
RUN apt-get -qq update \
 && apt-get -qq install -y git \
 && apt-get -qq clean
USER sage
RUN sage -pip install git+https://github.com/mkauers/ore_algebra.git
COPY --chown=sage:sage . ${HOME}
